import axios from "axios";
const api = axios.create({
    baseURL: "https://strapi-api-1.herokuapp.com",
});
export default api;